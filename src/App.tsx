declare function require(name: string): any;

import * as React from 'react';
import {Router} from 'react-router';

import routes from './routes';

export default class App extends React.Component<{history: any}, {}> {
    render() {
        return <Router history={this.props.history}>{routes}</Router>
    }
}