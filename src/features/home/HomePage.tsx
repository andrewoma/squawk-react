import * as React from "react";

import {HelloThere} from "../../components/Hello";
import {Example} from "../../components/ModalFun";

export default (props: any) => (
    <div>
        <HelloThere compiler="Foo2" framework="Bars">
            <Example/>
        </HelloThere>
    </div>
);
