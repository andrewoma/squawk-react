import * as React from "react"
import {TextField, NumberField, parseDate, formatDate, DateField, parseNumber, validate} from "../../components/Form"
import {Field, FormProps, reduxForm} from 'redux-form'
import {User, Address} from './UserContainer'
import {Button} from 'react-bootstrap'

class UserForm extends React.Component<FormProps<User, {}>, {}> {
    render() {
        const {handleSubmit, pristine, reset, submitting} = this.props;
        return <form onSubmit={handleSubmit}>
            <Field name="username" component={TextField} label="Username"/>
            <Field name="email" component={TextField} label="Email"/>
            <Field name="age" component={NumberField} label="Age" parse={parseNumber}/>
            <Field name="dob" component={DateField} label="Date of Birth" format={formatDate} parse={parseDate}/>

            <Field name="address.line1" component={TextField} label="Line 1"/>
            <Field name="address.line2" component={TextField} label="Line 2"/>

            <div>
                <Button type="submit" bsStyle="primary" disabled={submitting}>Submit</Button>
                <Button type="button" bsStyle="default" className="pull-right" disabled={pristine || submitting}
                        onClick={reset}>Reset</Button>
            </div>
        </form>
    }
}

const validateAddress = (address: Address) => validate(address, {
    line1: {
        presence: true,
        length: {minimum: 20},
    },
});

const validateUser = (user: User) => {
    let errors = validate(user, {
        username: {
            presence: true,
            length: {maximum: 10},
        },
        email: {
            presence: true,
            email: {}
        },
        age: {
            presence: true,
            numericality: {onlyInteger: true, strict: true, greaterThan: 17}
        },
        date: {
            presence: true,
        },
    });
    errors.address = validateAddress(user.address);
    return errors
};

const warnUser = (user: User) => validate(user, {
    age: {
        numericality: {greaterThan: 18, message: "seems a bit young..."}
    },
});

declare const module: any;

export default reduxForm({
    initialValues: {
        username: "andrewo",
        email: "andrew@omalley.id.au",
        age: 18,
        dob: "2016-02-01",
        address: {
            line1: "Level 16",
        }
    },
    destroyOnUnmount: !module.hot,
    form: 'syncValidation',
    validate: validateUser,
    warn: warnUser
})(UserForm)
