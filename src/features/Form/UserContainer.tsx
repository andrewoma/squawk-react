import * as React from "react";
import UserForm from './UserForm';
import {Well} from 'react-bootstrap';

export interface Address {
    line1: String
    line2?: String
}

export interface User {
    username: string
    email: string
    age: number
    dob: string
    address: Address
}

export default class ValidatingFormView extends React.Component<any, any> {
    handleSubmit = (values: User) => {
        console.log("Submit", values);
    };

    render() {
        return <div className="row">
            <div className="col-lg-6">
                <Well>
                    <UserForm onSubmit={this.handleSubmit}/>
                </Well>
            </div>
        </div>;
    }
}
