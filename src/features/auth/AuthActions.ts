import actionCreatorFactory from "redux-typescript-actions";
import {Dispatch} from "react-redux";
import {SubmissionError} from "redux-form";

const actionCreator = actionCreatorFactory('App/Auth');

export type AuthState = {
    token?: string,
    username?: string
};

export type Login = {
    username: string
    password: string
}

export const loginSuccess = actionCreator<AuthState>('LOGIN_SUCCESS');

type RequestMethod = "get" | "post" | "put" | "delete" | "patch";

interface JsonResponse {
    status: number,
    ok: boolean,
    response: any,
    method: RequestMethod
}

function jsonRequest(url: string, body?: any, method?: RequestMethod): Promise<JsonResponse> {
    const request = new Request(url, {
        body: body === null ? null : JSON.stringify(body),
        method: method ? method : (body ? "post" : "get"),
        headers: new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }),
        credentials: "same-origin",
        mode: "same-origin",
        cache: "no-cache",
        referrerPolicy: "no-referrer"
    });

    return fetch(request)
        .then<JsonResponse>(response => {
            const length = response.headers.get("Content-Length");
            if (response.status === 204 || Number(length) === 0) {
                return Promise.resolve({status: response.status, ok: response.ok, response: null, method: request.method})
            } else {
                return response.json().then(json => ({status: response.status, ok: response.ok, response: json, method: request.method}));
            }
        })
}

function appRequest<T>(dispatch: Dispatch<any>, url: string, body?: any, method?: RequestMethod): Promise<T> {
    const appUrl = "http://localhost:9898/api" + url;
    return jsonRequest(appUrl, body, method)
        .then(response => {
            if (response.ok) {
                return Promise.resolve(response.response as T);
            } else if (response.status === 401) {
                dispatch(logout)
            } else if (response.method == "get" && response.status === 404) {
                return Promise.resolve(null)
            } else {
                return Promise.reject(response.response)
            }
        })
}

export const login = function (login: Login) {

    return (dispatch: Dispatch<any>) => {
        return jsonRequest("/login", {name: login.username, password: login.password}).then(response => {
                if (response.ok) {
                    dispatch(loginSuccess({username: login.username}));
                } else {
                    throw new SubmissionError({_error: "Bugger"})
                }
            }
        );
    }
};

export const logout = actionCreator<{}>('LOGOUT');