import * as React from "react";
import LoginForm from './LoginForm'

export default (props: any) => (
    <div className="row">
        <div className="col-lg-offset-4 col-lg-4">
            <LoginForm/>
        </div>
    </div>
);