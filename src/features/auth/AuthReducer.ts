import {Action} from 'redux'
import {isType} from 'redux-typescript-actions'
import * as auth from './AuthActions'

export const reducer = (state: auth.AuthState = {}, action: Action): auth.AuthState => {

    if (isType(action, auth.loginSuccess)) {
        return action.payload
    }

    if (isType(action, auth.logout)) {
        return {}
    }

    return state;
};