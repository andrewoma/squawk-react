import * as React from "react"
import {TextField, validate} from "../../components/Form"
import {Field, FormProps, reduxForm} from 'redux-form'
import {Alert, Button, Well} from 'react-bootstrap'
import * as auth from "./AuthActions";
import {asyncDispatch} from '../../utils/utils'
import {Dispatch} from "react-redux"
import {withRouter} from 'react-router'

const Form = (props: FormProps<auth.Login, {}>) => {
    const {error, handleSubmit, submitting} = props;
    return <Well>
        <form onSubmit={handleSubmit}>
            <Field name="username" component={TextField} label="Username"/>
            <Field name="password" component={TextField} type="password" label="Password"/>

            {error && <Alert bsStyle="danger">{error}</Alert>}

            <div>
                <Button type="submit" bsStyle="primary" disabled={submitting}>Login</Button>
            </div>
        </form>
    </Well>
};

const validateLogin = (login: auth.Login) => validate(login, {
    username: {presence: true},
    password: {presence: true},
});

const LoginForm = reduxForm({
    initialValues: {
        username: 'andrewo',
        password: 'password123'
    },
    form: 'loginForm',
    validate: validateLogin,
})(Form as any);

class LoginContainer extends React.Component<any , {}> {

    handleSubmit = (login: auth.Login, dispatch: Dispatch<any>) => {
        const redirect = this.props.location.query.redirect;
        const router = this.props.router;

        return asyncDispatch(dispatch, auth.login(login)) .then(() =>
            router.push(redirect || '/'));
    };

    render() {
        return <LoginForm onSubmit={this.handleSubmit}/>
    }
}

export default withRouter(LoginContainer);