import * as React from "react"
import {Alert} from 'react-bootstrap'

export default (props: any) => (
    <div>
        <Alert bsStyle="danger">Sorry, your page wasn't found</Alert>
    </div>
);
