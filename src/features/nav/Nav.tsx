import * as React from "react"

import {Nav as BsNav, Navbar, NavDropdown, MenuItem, NavItem} from 'react-bootstrap'
import {IndexLinkContainer, LinkContainer} from 'react-router-bootstrap'
import {connect} from 'react-redux'
import * as auth from '../auth/AuthActions'

interface NavProps {
    auth: auth.AuthState
    onLogout: () => void
    children: any
}

const Nav = (props: NavProps) => (
    <div className="container">
        <Navbar fluid>
            <Navbar.Header>
                <Navbar.Brand>
                    <a href="#">Change Up</a>
                </Navbar.Brand>
            </Navbar.Header>

            <BsNav pullRight>
                <IndexLinkContainer to="/">
                    <NavItem>Home</NavItem>
                </IndexLinkContainer>
                <LinkContainer to="/users">
                    <NavItem>Users</NavItem>
                </LinkContainer>
                <LinkContainer to="/about">
                    <NavItem>About</NavItem>
                </LinkContainer>
                {
                    props.auth.username
                        ?
                        <NavDropdown title={props.auth.username} id="basic-nav-dropdown">
                            <MenuItem onClick={props.onLogout}>Logout</MenuItem>
                        </NavDropdown>
                        :
                        <LinkContainer to="/login">
                            <NavItem >Login</NavItem>
                        </LinkContainer>
                }
            </BsNav>
        </Navbar>
        {props.children}
    </div>
);

const mapStateToProps = (state: any) => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch: any) => ({
    onLogout: () => {
        dispatch(auth.logout({}))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Nav);