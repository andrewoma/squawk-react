import * as React from "react";

import {Button, Modal} from "react-bootstrap";

export interface ExampleState {
    showModal: boolean
}

export class Example extends React.Component<{}, ExampleState> {
    constructor(props: {}) {
        super(props);
        this.state = {showModal: false};
    }

    close() {
        this.setState({showModal: false});
    }

    open() {
        this.setState({showModal: true});
    }

    public render() {
        return (
            <div>
                <p>Click to get the full Modal experience!</p>

                <Button
                    bsStyle="primary"
                    bsSize="large"
                    onClick={e => this.open()}>
                    Launch demo modal
                </Button>

                <Modal show={this.state.showModal} onHide={() => this.close()}>
                    <Modal.Header closeButton>
                        <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h4>Text in a modal</h4>
                        <hr />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={e => this.close()}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}