declare function require(name: string): any;
import * as React from "react";
import {FormGroup, ControlLabel, FormControl, HelpBlock} from 'react-bootstrap'
import * as DateTimePicker from 'react-widgets/lib/DateTimePicker'
import * as moment from 'moment'
import Constraints = ValidateJS.Constraints;

const validateJs = require('validate.js');
const omit = require('object.omit');

export function validate(value: any, constraints: Constraints): any {
    return validateJs(value, constraints) || {}
}

const BootstrapField = (field: any) => {
    const {label, meta: {touched, error, warning}} = field;

    // console.log("errors", error);
    const showErrors = touched && (error || warning);
    let key = 0;
    const help = showErrors ?
        <HelpBlock>
            {(error || []).map((error: any) => <div key={++key}>{error}</div>)}
            {(warning || []).map((warning: any) => <div key={++key}>{warning}</div>)}
        </HelpBlock>
        : <span/>;

    return (
        <FormGroup className={ touched && ((error && "has-error") || (warning && "has-warning"))}>
            <ControlLabel>{label}</ControlLabel>
            { React.cloneElement(field.children, omit(field, "children", "meta", "input")) }
            { help }
        </FormGroup>
    );
};

export const TextField = (field: any) =>
    <BootstrapField {...field}>
        <FormControl type="text" {...field.input}/>
    </BootstrapField>;

export const NumberField = (field: any) =>
    <BootstrapField {...field}>
        <FormControl type="number" {...field.input}/>
    </BootstrapField>;

export const DateField = (field: any) => {
    function onChange(date: Date, formatted: string) {
        const changeValue = date === null ? null : formatted;
        field.input.onChange(changeValue);
    }

    return <BootstrapField {...field}>
        <DateTimePicker format="D/M/YYYY" time={false}
                        onChange={onChange} {...omit(field.input, "onChange") } />
    </BootstrapField>;
};

export function formatDate(iso: string): Date {
    return iso ? moment(iso).toDate() : null;
}

export function parseDate(val: string) {
    const value = moment(val, "D/M/YYYY", true);
    return value.isValid() ? value.format("YYYY-MM-DD") : null;
}

export function parseNumber(val: string) {
    return val ? Number(val) : null
}
