import * as React from "react";

export interface HelloProps {
    compiler: string;
    framework: string
}

export class HelloThere extends React.Component<HelloProps, {}> {
    render() {
        return <div>
            <h4>Hello  {this.props.compiler} and {this.props.framework}!</h4>
            <div>{this.props.children}</div>
        </div>
    }
}