import App from "./App";
import thunk from 'redux-thunk';

declare function require(name: string): any;
declare const __DEV__: boolean; // from webpack
declare const module: any; // from webpack

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Router, hashHistory} from 'react-router';
import {Provider} from 'react-redux';
import {syncHistoryWithStore, routerMiddleware, routerReducer} from 'react-router-redux'
import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
const AppContainer = require('react-hot-loader').AppContainer;

import reducer from './reducers'

// Initialise react-widgets to use Moment localisers
const moment = require("moment");
const momentLocalizer = require("react-widgets/lib/localizers/moment");
momentLocalizer(moment);

const middleware = routerMiddleware(hashHistory);

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(reducer, composeEnhancers(
    applyMiddleware(middleware, thunk)
));

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(hashHistory, store);

const rootEl = document.getElementById('squawk');

ReactDOM.render(
    <AppContainer>
        <Provider store={store}>
            <App history={history}/>
        </Provider>
    </AppContainer>, rootEl);

if (module.hot) {
    module.hot.accept('./App', () => {
        // If you use Webpack 2 in ES modules mode, you can
        // use <App /> here rather than require() a <NextApp />.
        const NextApp = require('./App').default;
        ReactDOM.render(
            <AppContainer>
                <Provider store={store}>
                    <NextApp history={history}/>
                </Provider>
            </AppContainer>,
            rootEl
        );
    });
}