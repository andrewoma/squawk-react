
import {Dispatch} from "react-redux";

export function log<T>(message: string, val: T): T {
    console.log(message, val);
    return val
}

export function asyncDispatch<T>(dispatch: Dispatch<any>, thunk: (dispatch: Dispatch<any>) => Promise<T>) {
    return dispatch(thunk as any) as Promise<T>
}