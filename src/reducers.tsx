import { reducer as formReducer } from 'redux-form'
import { reducer as authReducer } from './features/auth/AuthReducer'
import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

export default combineReducers({
    auth: authReducer,
    form: formReducer,
    routing: routerReducer,
});
