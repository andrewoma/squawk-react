declare function require(name: string): any;
import * as React from 'react';
import {Route, IndexRoute} from 'react-router';

import Nav from './features/nav/Nav';
import NotFoundPage from './features/nav/NotFoundPage';
import HomePage from './features/home/HomePage';
import AboutPage from './features/about/AboutPage';
import UserContainer from './features/Form/UserContainer'
import LoginPage from './features/auth/LoginPage'
import {routerActions} from 'react-router-redux'
const {UserAuthWrapper} =  require('redux-auth-wrapper');

// Redirects to /login by default
const UserIsAuthenticated = UserAuthWrapper({
    authSelector: (state: any) => (state.auth),
    redirectAction: routerActions.replace, // Replace on redirect
    wrapperDisplayName: 'UserIsAuthenticated'
});

const routes = (
    <Route path="/" component={Nav}>
        <IndexRoute component={HomePage}/>
        <Route path="/about" component={AboutPage}/>
        <Route path="/users" component={UserIsAuthenticated(UserContainer)}/>
        <Route path="/login" component={LoginPage}/>
        <Route path="*" component={NotFoundPage}/>
    </Route>
);

export default routes;