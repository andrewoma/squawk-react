package squawk.server

import java.time.LocalDate


data class User(
        val id: Int = 0,
        val name: String,
        val dob: LocalDate,
        val email: String,
        val password: String,
        val address: Address?,
        val tags: List<String> = listOf()
)

data class Address(
        val line1: String,
        val line2: String?,
        val postCode: String
)
