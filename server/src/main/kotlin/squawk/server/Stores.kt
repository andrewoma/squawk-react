package squawk.server

import com.fasterxml.jackson.databind.ObjectMapper
import java.util.*

abstract class Store<T : Any>(val id: (T) -> Int, val type: Class<T>) {
    private var valuesById = mutableMapOf<Int, T>()
    protected var maxId: Int = 0

    open fun onCreate(value: T): T = value
    open fun onUpdate(existing: T, update: T): T = update
    open fun onView(value: T): T = value

    fun create(t: T): T {
        val created = onCreate(t)
        valuesById[id(created)] = created
        return created
    }

    fun update(t: T): T {
        val old = valuesById[id(t)]
        val updated = onUpdate(old!!, t)
        valuesById[id(updated)] = updated
        return t
    }

    fun delete(id: Int): T? = valuesById.remove(id)

    fun load(mapper: ObjectMapper): Store<T> {
        javaClass.classLoader.getResourceAsStream(type.simpleName.toLowerCase() + "s.json").bufferedReader().use { reader ->
            val list = mapper.readValue<List<T>>(reader, mapper.typeFactory.constructCollectionType(List::class.java, type))
            valuesById = TreeMap(list.associateBy { id(it) })
            maxId = list.map { id(it) }.max() ?: 0
            return this
        }
    }

    val values: Collection<T>
        get() = valuesById.values

    operator fun get(id: Int) = valuesById[id]
}

class Stores(mapper: ObjectMapper) {

    val users = object : Store<User>(User::id, User::class.java) {

        override fun onCreate(value: User): User {
            validate(!values.any { it.name == value.name }, ErrorCode.USERNAME_CONFLICT) { "User exists with name '${value.name}'" }
            validate(!values.any { it.email == value.email }, ErrorCode.EMAIL_CONFLICT) { "User exists with email '${value.email}'" }
            return value.copy(id = ++maxId)
        }

        override fun onView(value: User) = value.copy(password = "***")

    }.load(mapper)
}

