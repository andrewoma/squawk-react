package squawk.server

import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.content.HttpStatusCodeContent
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.routing.*


private suspend fun <R, T : Any> entityOrNotFound(store: Store<T>, call: ApplicationCall, onFound: suspend (T) -> R) {
    val id = call.parameters["id"]
    val entity = id?.toIntOrNull()?.let { store[it] }
    if (entity == null) {
        call.toJson(Error(ErrorCode.NOT_FOUND, "id of $id not found in store"), HttpStatusCode.NotFound)
    } else {
        onFound(entity)
    }
}

fun <T : Any> Route.crud(name: String, store: Store<T>) {

    get(name) {
        call.toJson(store.values.map { store.onView(it) })
    }

    get("$name/{id}") {
        entityOrNotFound(store, call) { value ->
            call.toJson(store.onView(value))
        }
    }

    put("$name/{id}") {
        entityOrNotFound(store, call) { _ ->
            call.toJson(store.onView(store.update(call.fromJson<T>(store.type))))
        }
    }

    delete("$name/{id}") { _ ->
        entityOrNotFound(store, call) { value ->
            store.delete(store.id(value))
            call.respond(HttpStatusCodeContent(HttpStatusCode.NoContent))
        }
    }

    post(name) {
        call.toJson(store.onView(store.create(call.fromJson<T>(store.type))))
    }
}

