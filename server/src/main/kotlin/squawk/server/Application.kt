package squawk.server

import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.application.ApplicationCallPipeline
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.application.install
import org.jetbrains.ktor.auth.*
import org.jetbrains.ktor.features.StatusPages
import org.jetbrains.ktor.host.embeddedServer
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.jetty.Jetty
import org.jetbrains.ktor.logging.CallLogging
import org.jetbrains.ktor.pipeline.PipelineContext
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.get
import org.jetbrains.ktor.routing.post
import org.jetbrains.ktor.routing.route
import org.jetbrains.ktor.routing.routing
import org.jetbrains.ktor.sessions.*
import org.jetbrains.ktor.util.nextNonce
import org.springframework.security.crypto.bcrypt.BCrypt

data class Session(val id: String, val user: String)

fun main(args: Array<String>) {

    val hashKey = "supersecretkey".toByteArray(Charsets.UTF_8)
    val mapper = jacksonObjectMapper()
    val stores = Stores(mapper)

    val server = embeddedServer(Jetty, port = 9797) {

        intercept(ApplicationCallPipeline.Infrastructure) { it.attributes.put(JacksonAttribute, mapper) }
        install(CallLogging)
        install(StatusPages) {
            jacksonExceptionMappers()
            validationExceptionMapper()
            exception<Throwable> { e ->
                e.printStackTrace()
                call.toJson(Error(ErrorCode.INTERNAL_SERVER_EXCEPTION, e.message ?: ""), HttpStatusCode.InternalServerError)
            }
        }

        withSessions<Session> {
            withCookieByValue {
                settings = SessionCookiesSettings(transformers = listOf(SessionCookieTransformerMessageAuthentication(hashKey)))
            }
        }

        routing {

            post("/api/login") {
                login(stores)
            }

            route("api") {
                authentication {
                    intercept(AuthenticationPipeline.RequestAuthentication) { context ->
                        if (context.call.sessionOrNull<Session>() == null) {
                            context.call.respond(UnauthorizedResponse(*emptyArray()))
                        }
                    }
                }

                crud("users", stores.users)
            }

            get("/") {
                call.respondText(javaClass.getResource("/static/index.html").readText(), ContentType.Text.Html)
            }
        }
    }

    server.start()
}

private suspend fun PipelineContext<ApplicationCall>.login(stores: Stores) {
    val login = call.fromJson<UserPasswordCredential>()
    val user = stores.users.values.firstOrNull { it.name == login.name }
    if (user != null && BCrypt.checkpw(login.password, user.password)) {
        call.session(Session(nextNonce(), login.name))
        call.toJson(UserIdPrincipal(login.name))
    } else {
        call.clearSession()
        call.toJson(Error(ErrorCode.UNAUTHORISED, "Invalid user or password"), HttpStatusCode.BadRequest)
    }
}
