package squawk.server

import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.features.StatusPages
import org.jetbrains.ktor.http.HttpStatusCode


enum class ErrorCode {
    UNAUTHORISED,
    NOT_FOUND,
    PARAMETER_REQUIRED,
    INTERNAL_SERVER_EXCEPTION,
    BAD_REQUEST,
    USERNAME_CONFLICT,
    EMAIL_CONFLICT,
}

class Error(
        val code: ErrorCode,
        val message: String
)

class ValidationException(val error: Error) : RuntimeException("${error.code}: ${error.message}")

inline fun validate(condition: Boolean, code: ErrorCode, message: () -> String) {
    if (!condition) {
        throw ValidationException(Error(code, message()))
    }
}

fun StatusPages.Configuration.validationExceptionMapper() {
    exception<ValidationException> { e ->
        call.toJson((e as ValidationException).error, HttpStatusCode.BadRequest)
    }
}
