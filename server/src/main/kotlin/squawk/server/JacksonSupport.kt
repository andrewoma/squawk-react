package squawk.server

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.features.StatusPages
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.util.AttributeKey

object JacksonAttribute : AttributeKey<ObjectMapper>("Jackson")

inline suspend fun <reified T : Any> ApplicationCall.fromJson(): T {
    val mapper = this.attributes[JacksonAttribute]
    return mapper.readValue(this.request.receive(String::class), T::class.java)
}

suspend fun <T : Any> ApplicationCall.fromJson(clazz: Class<T>): T {
    val mapper = this.attributes[JacksonAttribute]
    return mapper.readValue(this.request.receive(String::class), clazz)
}

suspend fun ApplicationCall.toJson(data: Any, status: HttpStatusCode = HttpStatusCode.OK) {
    val mapper = this.attributes[JacksonAttribute]
    this.response.status(status)
    this.respondText(mapper.writeValueAsString(data), ContentType.Application.Json)
}

fun jacksonObjectMapper() = ObjectMapper()
        .registerModule(KotlinModule())
        .registerModule(JavaTimeModule())
        .enable(SerializationFeature.INDENT_OUTPUT)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .setSerializationInclusion(JsonInclude.Include.NON_NULL)

fun StatusPages.Configuration.jacksonExceptionMappers() {
    exception<MissingKotlinParameterException> { e ->
        val name = (e as MissingKotlinParameterException).parameter.name
        call.toJson(Error(ErrorCode.PARAMETER_REQUIRED, "'$name' is required"), HttpStatusCode.BadRequest)
    }
    exception<JsonMappingException> { e ->
        call.toJson(Error(ErrorCode.BAD_REQUEST, e.message ?: ""), HttpStatusCode.BadRequest)
    }
}

