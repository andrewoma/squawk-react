var http = require('http');

var express = require('express');

var app = express();

var proxy = require('http-proxy-middleware');

app.use(require('morgan')('short'));

// ************************************
// This is the real meat of the example
// ************************************
(function () {

    // Step 1: Create & configure a webpack compiler
    var webpack = require('webpack');
    var webpackConfig = require(process.env.WEBPACK_CONFIG ? process.env.WEBPACK_CONFIG : './webpack.config');
    var compiler = webpack(webpackConfig);

    // Step 2: Attach the dev middleware to the compiler & the server
    app.use(require("webpack-dev-middleware")(compiler, {
        noInfo: false, publicPath: webpackConfig.output.publicPath
    }));

    // Step 3: Attach the hot middleware to the compiler & the server
    app.use(require("webpack-hot-middleware")(compiler, {
        log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
    }));
})();

app.get("/", function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.use('/api', proxy({target: 'http://localhost:9797', changeOrigin: true}));

app.use(express.static('.'));

if (require.main === module) {
    var server = http.createServer(app);
    server.listen(process.env.PORT || 9898, function () {
        console.log("Listening on %j", server.address());
    });
}