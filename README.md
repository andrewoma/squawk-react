### Building

`yarn install` 

##### Auto refreshing dev server

`npm run dev` -> [http://localhost:9898]()

##### Prod build

`npm run prod` -> `dist/bundle.js`
