const webpack = require('webpack');

const applicationEntries = process.env.NODE_ENV === 'dev'
    ? ['webpack-hot-middleware/client?reload=true']
    : [];

module.exports = {
    entry: ['webpack-hot-middleware/client', 'react-hot-loader/patch', "./src/squawk.tsx"],
    // entry: ["./src/squawk.tsx"].concat(applicationEntries),
    output: {
        filename: "bundle.js",
        publicPath: "/dist/",
        path: __dirname + "/dist"
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        loaders: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            {test: /\.tsx?$/, exclude: /node_modules/, loaders: ['react-hot-loader/webpack', "ts-loader"]}
        ],

        preLoaders: [
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            {test: /\.js$/, exclude: /node_modules/, loaders: ["source-map-loader"]}
        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            __DEV__: process.env.NODE_ENV === 'dev',
            __TEST__: process.env.NODE_ENV === 'test',
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
    ],

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
};